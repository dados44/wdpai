create table users
(
    id              serial
        constraint users_pk
            primary key,
    email           varchar(255)      not null,
    password        varchar(255)      not null,
    id_user_details integer default 0 not null
        constraint details_users___fk
            references users_details
            on update cascade on delete cascade,
    role            integer
);

alter table users
    owner to snjvgkycnewush;

create unique index users_id_uindex
    on users (id);

INSERT INTO public.users (id, email, password, id_user_details, role) VALUES (11, 'jopek@dawid.pl', '21232f297a57a5a743894a0e4a801fc3', 13, 2);
INSERT INTO public.users (id, email, password, id_user_details, role) VALUES (12, 'dawid@jopek.pl', '21232f297a57a5a743894a0e4a801fc3', 15, 1);
INSERT INTO public.users (id, email, password, id_user_details, role) VALUES (13, 'maciej@marek.pl', '21232f297a57a5a743894a0e4a801fc3', 16, 1);
INSERT INTO public.users (id, email, password, id_user_details, role) VALUES (15, 'michal@mak.pl', '21232f297a57a5a743894a0e4a801fc3', 18, 2);


create table users_details
(
    id      integer default nextval('user_details_id_seq'::regclass) not null
        constraint user_details_pk
            primary key,
    name    varchar(100)                                             not null,
    surname varchar(100)                                             not null
);

alter table users_details
    owner to snjvgkycnewush;

create unique index user_details_id_uindex
    on users_details (id);

INSERT INTO public.users_details (id, name, surname) VALUES (13, 'Dawid', 'Jopek');
INSERT INTO public.users_details (id, name, surname) VALUES (15, 'Radek', 'Kotarek');
INSERT INTO public.users_details (id, name, surname) VALUES (16, 'Maciej', 'Marek');
INSERT INTO public.users_details (id, name, surname) VALUES (18, 'Michal', 'Mak');



create table excercises
(
    id          serial
        constraint excercises_pk
            primary key,
    title       varchar(100) not null,
    description varchar(300) not null,
    id_added_by integer      not null
        constraint excercises_users_id_fk
            references users
            on update cascade on delete cascade,
    created_at  date         not null,
    image       varchar(255),
    status      boolean,
    response    varchar
);

alter table excercises
    owner to snjvgkycnewush;

create unique index excercises_id_uindex
    on excercises (id);

INSERT INTO public.excercises (id, title, description, id_added_by, created_at, image, status, response) VALUES (51, 'Pomoc z OHP', 'mam problem z ćwiczeniem OHP, ktoś pomoże?', 11, '2022-06-29', 'gf-yqAk-4LhB-ZgAi_wyciskanie-zolnierskie-1920x1080-nocrop.jpg', null, null);
INSERT INTO public.excercises (id, title, description, id_added_by, created_at, image, status, response) VALUES (52, 'Potrzebuje rady na ławe!', 'Mam problem z wykonaniem wyciskania na klate, co robię źle?', 11, '2022-06-29', 'wyciskanie-na-klatke1-9181c1b69ccc0ef814111b0413f07381.jpg', null, null);
INSERT INTO public.excercises (id, title, description, id_added_by, created_at, image, status, response) VALUES (55, 'Pomoc z prysiadem ze sztanga', 'Pomoc pomoc 123 pomocy z przysiadem :)', 11, '2022-06-29', 'mh_silka02637-491f22b0dfd8195c8195bb9bb4bedd0c.jpg', null, null);
INSERT INTO public.excercises (id, title, description, id_added_by, created_at, image, status, response) VALUES (56, 'Pomoc z cwiczeniem', 'Prosze o pomoc', 15, '2022-06-29', 'rozgrzewka-dynamiczna-wykroki-z-rotacja-tulowia.png', null, null);


create table users_excercises
(
    id_user      integer not null
        constraint user_users_excercise___fk
            references users
            on update cascade on delete cascade,
    id_excercise integer not null
        constraint excercise_excercise___fk
            references excercises
            on update cascade on delete cascade
);

alter table users_excercises
    owner to snjvgkycnewush;

create table coach_ratings
(
    id          serial
        constraint coach_ratings_pk
            primary key,
    rating      integer,
    expirience  varchar(255),
    coach_since date,
    id_user     integer not null
        constraint coach_ratings_users_id_fk
            references users
            on update cascade on delete cascade
);

alter table coach_ratings
    owner to snjvgkycnewush;

create unique index coach_ratings_id_uindex
    on coach_ratings (id);

INSERT INTO public.coach_ratings (id, rating, expirience, coach_since, id_user) VALUES (1, 10, 'trenowal Ronaldo', '2022-06-28', 12);
INSERT INTO public.coach_ratings (id, rating, expirience, coach_since, id_user) VALUES (2, 15, 'trenowal Messiego', '2022-06-13', 13);



