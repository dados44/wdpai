const search = document.querySelector('input[placeholder="SEARCH"]');
const excerciseContainer = document.querySelector(".excersiceRequest");

search.addEventListener("keyup", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();

        const data = {search: this.value};
        // console.log(JSON.stringify(data));
        // console.log(excerciseContainer);

        fetch("/search", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
           // console.log("responisisisisi: ",response.json())
            return response.json();
        }).then(function (excercises) {
            excerciseContainer.innerHTML = "";
            loadExcercises(excercises)
        });
    }
});

function loadExcercises(excercises) {
    excercises.forEach(excercice => {
        console.log(excercice);
        createExcercise(excercice);
    });
}

function createExcercise(excercice) {
    const template = document.querySelector("#excerciseTemplate");

    const clone = template.content.cloneNode(true);
    const div = clone.querySelector("div");
    div.id = excercice.id;
    const image = clone.querySelector("img");
    image.src = `/public/uploads/${excercice.image}`;
    const title = clone.querySelector("h2");
    title.innerHTML = excercice.title;
    const description = clone.querySelector("p");
    description.innerHTML = excercice.description;
    excerciseContainer.appendChild(clone);
}



