<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/coachView.css">
    <link rel="stylesheet" type="text/css" href="public/css/profile.css">
    <script src="https://kit.fontawesome.com/85633f9409.js" crossorigin="anonymous"></script>
    <title>bHoH - profile</title>
</head>
<body>
<div class="head-container">
    <nav>

        <?php include('usages/navMenu.php') ?>

    </nav>
    <main>
        <?php include('usages/header.php') ?>

            <?php foreach ($coaches as $coach): ?>

                    <div>
                        <h1>Coach id: <?= $coach->getId() ?></h1>
                        <p>Has rating: <?= $coach->getRating() ?></p>
                        <p>And has expirience:<?=  $coach->getExpirience()?> </p>
                    </div>
            <?php endforeach; ?>

    </main>

</div>
<?php include('usages/footer.php') ?>

</body>
