<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/coachView.css">
    <link rel="stylesheet" type="text/css" href="public/css/add_excercise.css">
    <script src="https://kit.fontawesome.com/85633f9409.js" crossorigin="anonymous"></script>
    <title>bHoH - User</title>
</head>
<body>
    <div class="head-container">
        <nav>
            <?php include('usages/navMenu.php') ?>
            
        </nav>
        <main>
            <?php include('usages/header.php') ?>
            <section class="addExcercise_view">
                <div>

                <form class="add_Excercise" action="add_Excercise" method="post" enctype="multipart/form-data">
                    <h1>Send a Video and description of Exercise</h1>
                    <?php if(isset($messages)) {
                        foreach ($messages as $message){
                            echo $message;
                        }

                    }
                    ?>

                    <input name="title" type="text" placeholder="title">
                    <textarea name="description" rows="5" placeholder="Excercise description"></textarea>
                    <input name="file" type="file">
                    <button type="submit">Send</button>
                </form>
                </div>
            </section>

        </main>
        
    </div>
    <?php include('usages/footer.php') ?>
    
</body>
