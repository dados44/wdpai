<?php
if (isset($_COOKIE['user'])) {
    unset($_COOKIE['key']);
    setcookie('user', '', time() - 3600, '/'); // empty value and old timestamp
    setcookie('role', '', time() - 3600, '/'); // empty value and old timestamp
}

$url = "http://$_SERVER[HTTP_HOST]";
header("Location: {$url}/login");