<!DOCTYPE html>
<head>
    <?php
    if(isset($_COOKIE['user'])){
        $url = "http://$_SERVER[HTTP_HOST]";
        //header("Location: {$url}/userView");
        if($_COOKIE['role']==2) header("Location: {$url}/userView");
        elseif($_COOKIE['role']==1) header("Location: {$url}/coachView");
        else header("Location: {$url}/logout");
    };
    ?>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/userActions.css">
    <title>bHoH - login</title>
</head>
<body>
    <div class="container">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="login-container">
            
            <form class="login" action="login" method="post">
                <div class="message">
                    <?php if(isset($messages)) {
                        foreach ($messages as $message){
                            echo $message;
                        }

                    }
                    ?>

                </div>
                <label for=""><b>LOGIN</b><hr></label>
                <label for="email">Email</label>
                <input name="email" type="text" placeholder="">
                <label for="password">Password</label>
                <input name="password" type="password" placeholder="">
                <button type="submit">LOGIN</button>
                <a href="register" class="button_register">REGISTER</a>
            </form>

        </div>

    </div>
</body>
