<?php
$url = "http://$_SERVER[HTTP_HOST]";
if(!isset($_COOKIE['user'])){
    header("Location: {$url}/login");
}
////    require_once __DIR__.'/../../src/repository/UserRepository.php';
////    $userRepository1 = new UserRepository();
////    $roleValue = $userRepository1->getUserRolebyMail($_COOKIE['user']);
////    if($roleValue==1)
////        header("Location: {$url}/coachView");
////    else
////        header("Location: {$url}/userView");


?>
<?php //include('usages/cookie_check.php') ?>

<!DOCTYPE html>
<head>

    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/coachView.css">
    <script src="https://kit.fontawesome.com/85633f9409.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/search.js" defer></script>
    <script type="text/javascript" src="./public/js/oneExcercise.js" defer></script>

    <title>bHoH - User</title>
</head>
<body>

<div class="head-container">
    <nav>
        <div class="role">
        </div>
        <?php include('usages/navMenu.php') ?>

    </nav>
    <main>
        <?php include('usages/header.php') ?>
        <section class="excersiceRequest">

            <?php foreach ($excercises as $excercise): ?>

                <div id="<?= $excercise->getId() ?>">

                    <img src="public/uploads/<?=$excercise->GetImage() ?>">
                    <div>
                        <h2 class="a_none"><?= $excercise->GetTitle() ?></h2>
                        <p><?= $excercise->GetDescription() ?></p>
                    </div>

                </div>

            <?php endforeach; ?>
        </section>
    </main>

</div>
<!--    <div id="footer"></div>-->
<?php include('usages/footer.php') ?>

</body>

<template id="excerciseTemplate">
    <div id="exe_1">
        <img src="">
        <div>
            <h2>title</h2>
            <p>description</p>
        </div>
    </div>
</template>
