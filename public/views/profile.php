<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/coachView.css">
    <link rel="stylesheet" type="text/css" href="public/css/profile.css">
    <script src="https://kit.fontawesome.com/85633f9409.js" crossorigin="anonymous"></script>
    <title>bHoH - profile</title>
</head>
<body>
    <div class="head-container">
        <nav>

            <?php include('usages/navMenu.php') ?>
            
        </nav>
        <main>
            <?php include('usages/header.php') ?>
            <section class="profile_view">
                <div class="user">
                    <div class="user-info">
                        <div class="name">
                            <p>Name: <?= $user->getName()." ".$user->getSurname(); ?></p>
                        </div>
                        <div class="user_info">
                            <p>Email: <?= $user->getEmail(); ?></p>
                            <p>Role: <?php
                                if($user->getRole()==1){
                                    echo "Coach";
                                }
                                else echo "User"
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        
    </div>
    <?php include('usages/footer.php') ?>
    
</body>
