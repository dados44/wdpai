<?php

$url = "http://$_SERVER[HTTP_HOST]";

if(!isset($_COOKIE['user'])){
    header("Location: {$url}/login");
}else {
    if($_COOKIE['role']==1) {
        header("Location: {$url}/coachView");
        die();
    }
    elseif($_COOKIE['role']==2) {
        header("Location: {$url}/userView");
        die();
    }
    else {
        header("Location: {$url}/logout");
        die();
    }
}




