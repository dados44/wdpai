<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/coachView.css">
    <script src="https://kit.fontawesome.com/85633f9409.js" crossorigin="anonymous"></script>
    <title>bHoH - User</title>
</head>
<body>
    <div class="head-container">
        <nav>
            <img src="public/img/logo_black.svg">


            <div class="add">
                <i class="fas fa-plus"></i>
                <a href="#" class="profile_button"> ADD EXCERSICE</a>
            </div>
            <ul>

                
                <li>
                    <a href="login" class="button">HOME</a>
                </li>
                <li>
                    <a href="#" class="button">EXERCISES</a>
                </li>
                <li>
                    <a href="#" class="button">WORKOUT MASTERS</a>
                </li>
            </ul>
            
        </nav>
        <main>
            <header>
                 <div class="profile">
                    <i class="fa-solid fa-bars"></i><a href="#" class="profile_button">PROFILE</a>
                     <a href="/logout" class="profile_button">LOGOUT</a>
                </div>
            </header>
            <section class="add_Excercise_form">
                <h1>Edit your excercise</h1>
                <form action="add_Excercise" method="post" enctype="multipart/form-data">
                    <?php if(isset($messages)) {
                        foreach ($messages as $message){
                            echo $message;
                        }

                    }
                    ?>

                    <input name="title" type="text" placeholder="title">
                    <textarea name="description" rows="5" placeholder="Excercise description"></textarea>
                    <input name="file" type="file">
                    <button type="submit">Send</button>
                </form>
            </section>
        </main>
        
    </div>
    <div id="footer"></div>
    
</body>
