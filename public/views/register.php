<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/userActions.css">
    <script type="text/javascript" src="./public/js/script.js" defer></script>

    <title>bHoH - register</title>
</head>

<body>
<div class="container">
    <div class="logo">
        <img src="public/img/logo.svg">
    </div>
    <div class="register-container">
        <form class="register" action="register" method="POST">
            <div class="messages">
                <?php
                if(isset($messages)){
                    foreach($messages as $message) {
                        echo $message;
                    }
                }
                ?>
            </div>
            <label for=""><b>REGISTER</b><hr></label>
            <label for="email">Email</label>
            <input name="email" type="text" >
            <label for="password">Password</label>
            <input name="password" type="password" >
            <label for="confirmedPassword">Confirm Password</label>
            <input name="confirmedPassword" type="password">
            <label for="name">Name</label>
            <input name="name" type="text" >
            <label for="surname">Surname</label>
            <input name="surname" type="text" >
            <button type="submit">REGISTER</button>
            <a href="login" class="button_register">LOGIN</a>
        </form>
    </div>
</div>
</body>