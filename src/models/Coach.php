<?php

class Coach
{
    private $rating;
    private $expirience;
    private $coach_since;
    private $id;

    public function __construct($rating, $expirience, $coach_since, $id=null)
    {
        $this->rating = $rating;
        $this->expirience = $expirience;
        $this->coach_since = $coach_since;
        $this->id = $id;
    }


    public function getId(): int
    {
        return $this->id;
    }


    public function setId(mixed $id): void
    {
        $this->id = $id;
    }

    public function getRating()
    {
        return $this->rating;
    }


    public function setRating($rating): void
    {
        $this->rating = $rating;
    }


    public function getExpirience()
    {
        return $this->expirience;
    }


    public function setExpirience($expirience): void
    {
        $this->expirience = $expirience;
    }


    public function getCoachSince()
    {
        return $this->coach_since;
    }

    public function setCoachSince($coach_since): void
    {
        $this->coach_since = $coach_since;
    }

}
