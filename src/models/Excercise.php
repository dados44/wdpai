<?php

class Excercise
{

    private $title;
    private $description;
    private $image;
    private $id;
    private $status;
    private $response;

    public function __construct($title, $description, $image, $id=null,$status=false,$response=null)
    {
        $this->title = $title;
        $this->description = $description;
        $this->image = $image;
        $this->id = $id;
        $this->status = $status;
        $this->response= $response;
    }


    public function getTitle() :string
    {
        return $this->title;
    }


    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function getDescription() :string
    {
        return $this->description;
    }


    public function setDescription(string $description)
    {
        $this->description = $description;
    }


    public function getImage()
    {
        return $this->image;
    }


    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }



}