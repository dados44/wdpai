<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Coach.php';
require_once __DIR__.'/../repository/CoachRepository.php';

class CoachController extends AppController
{
    private $CoachRepository;

    public function __construct()
    {
        parent::__construct();
        $this->CoachRepository= new CoachRepository();
    }


    public function workoutMasters(){
        $coaches =$this->CoachRepository-> getCoaches_all();
        $this->render('workoutMasters',['coaches' => $coaches]);
    }





}