<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../models/User.php';

class UserController extends AppController
{
    private $userRepository;
    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function profile(){
        $this->isCookie();
        $userEmail = $_COOKIE['user'];

        $user = $this->userRepository->getUser($userEmail);

        $this->render('profile', ['user' => $user]);
    }

}