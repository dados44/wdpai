<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Excercise.php';
require_once __DIR__.'/../repository/ExcerciseRepository.php';

class ExcerciseController extends AppController
{
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $messages = [];
    private $excerciseRepository;


    public function __construct()
    {
        parent::__construct();
        $this->excerciseRepository = new ExcerciseRepository();
    }

    public function userView(){
        $excercises =$this->excerciseRepository->getExcercises();
        $this->render('userView',['excercises' => $excercises]);
    }

    public function coachView(){
        $excercises =$this->excerciseRepository->getExcercises_all();
        $this->render('coachView',['excercises' => $excercises]);
    }



    public function add_Excercise(){

        if($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file']) ){

            move_uploaded_file(
                $_FILES['file']['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name']
            );

            $excercise = new Excercise($_POST['title'], $_POST['description'], $_FILES['file']['name']);
            $this->excerciseRepository->addExcercise($excercise);

            return $this->render('userView', [
                'messages' => $this->message,
                'excercises' => $this->excerciseRepository->getexcercises()
            ]);
        }

    $this->render("add_Excercise", ['messages' =>$this->messages]);
    }


    public function search()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

        if ($contentType === "application/json") {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->excerciseRepository->getExcerciseByTitle($decoded['search']));
        }
    }

    public function excerciseView(int $id){
        $this->excerciseRepository->excerciseView($id);
    }

    private function validate(array $file) : bool
    {
        if($file['file'] > self::MAX_FILE_SIZE){
            $this->messages[] = 'To large File';
            return false;
        }
        if(!isset($file['file']) && !in_array($file['type'], self::SUPPORTED_TYPES)){
            $this->messages[] = 'Wrong file type!';
            return false;
        }

        return true;

    }

}