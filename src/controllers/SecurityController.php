<?php

require_once 'AppController.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../models/User.php';

class SecurityController extends AppController
{

    private $userRepository;

    public function __construct(){

        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function login(){


        if (!$this->isPost()) {
            return $this->render('login');
        }

        $email = $_POST["email"];
        $password = md5($_POST['password']);

        $user = $this->userRepository->getUser($email);

        if (!$user) {
            return $this->render('login', ['messages' => ['Not found user!']]);
        }


        if ($user->getEmail() !== $email){
            return $this->render('login',['messages' => ['Email not found']]);
        }
        if ($user->getPassword() !== $password){
            return $this->render('login',['messages' => ['Wrong password!!!']]);
        }

       // return $this->render('coachView');
        $cookie_name = "user";
        $cookie_value = $email;
        setcookie($cookie_name,$cookie_value,time() +(86400*30),"/");

        $roleValue = $this->userRepository->getUserRolebyMail($cookie_value);
        setcookie('role',$roleValue,time() +(86400*30),"/");
        $url = "http://$_SERVER[HTTP_HOST]";
        if($roleValue==1)
            header("Location: {$url}/coachView");
        else
            header("Location: {$url}/userView");

    }
    public function register()
    {
        if (!$this->isPost()) {
            return $this->render('register');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $role = 2;

        if ($password !== $confirmedPassword) {
            return $this->render('register', ['messages' => ['Please provide proper password']]);
        }

        //TODO try to use better hash function
        $user = new User($email, md5($password), $name, $surname,$role);

        $this->userRepository->addUser($user);

        return $this->render('login', ['messages' => ['You\'ve been succesfully registrated!']]);
    }

    public function profile(){
        $this->isCookie();
        $userEmail = $_COOKIE['user'];

        $user = $this->userRepository->getUser($userEmail);

        $this->render('profile', ['user' => $user]);
    }

    public function logout(){
        return $this->render('logout', ['messages' => ['You\'ve been succesfully registrated!']]);
    }
}