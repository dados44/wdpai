<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Excercise.php';
require_once __DIR__.'/../repository/UserRepository.php';


class ExcerciseRepository extends Repository
{
    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }


    public function getExcercise(int $id): ?excercise
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $excercise = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($excercise == false) {
            return null;
        }

        return new Excercise(
            $excercise['title'],
            $excercise['description'],
            $excercise['image']
        );
    }


    public function getExcercises_all(): array {

        $result = [];

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM excercises
        ');
        $stmt->execute();
        $excercices_all = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($excercices_all as $excercice_all) {
            $result[] = new Excercise(
                $excercice_all['title'],
                $excercice_all['description'],
                $excercice_all['image'],
                $excercice_all['id']
            );

        }

        return $result;

    }

    public function getExcercises(): array
    {
        $result = [];


        $stmt2 = $this->database->connect()->prepare('
            SELECT id from users WHERE email = :tmp
        ');
        $stmt2->bindParam(':tmp', $_COOKIE["user"], PDO::PARAM_STR);
        $stmt2->execute();
        $id_added_by = $stmt2->fetch(PDO::FETCH_ASSOC)["id"];

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM excercises WHERE id_added_by = :tmp
        ');
        $stmt->bindParam(':tmp', $id_added_by, PDO::PARAM_INT);
        $stmt->execute();
        $excercices = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($excercices as $excercice) {
            $result[] = new Excercise(
                $excercice['title'],
                $excercice['description'],
                $excercice['image'],
                $excercice['id']
            );

        }

        return $result;
    }

    public function addExcercise(Excercise $excercise): void
    {
        $date = new DateTime();
        $stmt = $this->database->connect()->prepare('
            INSERT INTO excercises (title, description,image, id_added_by, created_at)
            VALUES (?, ?, ?, ?, ?)
        ');

        $id_added_by = $this->userRepository->getUserbyMail($_COOKIE['user']);

        $stmt->execute([
            $excercise->getTitle(),
            $excercise->getDescription(),
            $excercise->getImage(),
            $id_added_by,
            $date->format('Y-m-d')
        ]);
    }

    public function getExcerciseByTitle(string $searchString){

        $searchString = '%' . strtolower($searchString) . '%';

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM excercises WHERE LOWER(title) LIKE :search OR LOWER(description) LIKE :search
        ');
        $stmt->bindParam(':search', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function excerciseView(int $id){
//        $url = "http://$_SERVER[HTTP_HOST]";
//        header("Location: {$url}/excerciseView/{$id}");

    }


}