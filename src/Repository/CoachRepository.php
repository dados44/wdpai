<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Coach.php';
require_once __DIR__.'/../repository/UserRepository.php';


class CoachRepository extends Repository
{
    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }


    public function getCoaches_all(): array {

        $result = [];

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM coach_ratings ORDER BY rating DESC;
        ');
        $stmt->execute();
        $coaches = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($coaches as $coach) {
            $result[] = new Coach(
                $coach['rating'],
                $coach['expirience'],
                $coach['coach_since'],
                $coach['id'],
            );

        }

        return $result;

    }




}