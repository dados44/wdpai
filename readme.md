### Dawid Jopek - PROJECT WDPAI
### bHoH - virtual personal coach
###

## Project description

The project has new user registrations, divided into USER or COACH roles. Depending on the role, we have different functions. User can view only his exercises and add new ones, Coach can browse all exercises and search for them

## Project build with:

* PHP
* JAVASCRIPT
* Database Connection - POSTGRESQL

 ## INSTALLATION
------------
 
 * Download DockerDesktop from https://www.docker.com/
 *  use git clone to download repository
 *  Open PHP editor, (PHPStorm, VS Studio Code) and run command terminal: docker-compuse build
 *  After build run command docker-compose up and open your browser in http://localhost:8080/










