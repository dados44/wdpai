<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url($path, PHP_URL_PATH);


Routing::get('', 'DefaultController');
Routing::get('index', 'DefaultController');

Routing::get('coachView', 'ExcerciseController');
Routing::get('userView', 'ExcerciseController');
Routing::post('login', 'SecurityController');
Routing::post('logout', 'SecurityController');
Routing::post('register', 'SecurityController');
Routing::post('add_Excercise', 'ExcerciseController');

Routing::post('profile', 'SecurityController');
Routing::post('workoutMasters', 'CoachController');
Routing::post('search', 'ExcerciseController');
Routing::get('excerciseView', 'ExcerciseController');
Routing::run($path);